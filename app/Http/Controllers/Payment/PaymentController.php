<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Model\Payment\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function saveAmount(Request $request){

        $this->validate($request, [
            'amount' => 'required|numeric'
        ]);

         $payment = new Payment($request->all());
        if ($payment->save()){
            return response()->json('Data save successful', 201);
        }

        return response()->json('Bad request', 400);
    }

    public function getStat(){

        $total = Payment::sum('amount');
        $tosixty = Payment::where('created_at', '>', now()->subSeconds(60))->get()->sum('amount');

     //   return $tosixty;


            return response()->json(['total_amount'=> round($total, 2), 'average_amount' => round($tosixty, 2)], 200);



    }
}
